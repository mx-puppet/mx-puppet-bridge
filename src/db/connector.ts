/*
Copyright 2018, 2019 matrix-appservice-discord

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import * as prometheus from "prom-client";

type SQLTYPES = number | boolean | string | null;

export interface ISqlCommandParameters {
	[paramKey: string]: SQLTYPES | Promise<SQLTYPES>;
}

export interface ISqlRow {
	[key: string]: SQLTYPES;
}

export interface IDatabaseTransaction {
	type: string;

	Get(sql: string, parameters?: ISqlCommandParameters): Promise<ISqlRow | null>;
	All(sql: string, parameters?: ISqlCommandParameters): Promise<ISqlRow[]>;
	Run(
		sql: string,
		parameters?: ISqlCommandParameters,
		returnId?: string
	): Promise<number>;
	Exec(sql: string): Promise<void>;
}

export interface IDatabaseConnector {
	type: string;
	latency: prometheus.Histogram<string>;
	Open(): void;
	Transaction<T>(
		callback: (txn: IDatabaseTransaction) => Promise<T>
	): Promise<T>;
	Close(): Promise<void>;

	/**
	 * @deprecated For safe concurrent access, wrap with Transaction instead
	 * @param sql
	 * @param parameters
	 * @constructor
	 */
	Get(sql: string, parameters?: ISqlCommandParameters): Promise<ISqlRow | null>;
	/**
	 * @deprecated For safe concurrent access, wrap with Transaction instead
	 * @param sql
	 * @param parameters
	 * @constructor
	 */
	All(sql: string, parameters?: ISqlCommandParameters): Promise<ISqlRow[]>;
	/**
	 * @deprecated For safe insertion that may fail at some point, wrap with Transaction instead
	 * @param sql
	 * @param parameters
	 * @param returnId
	 * @constructor
	 */
	Run(
		sql: string,
		parameters?: ISqlCommandParameters,
		returnId?: string
	): Promise<number>;
	/**
	 * @deprecated For safe execution of arbitrary SQL, wrap with Transaction instead
	 * @param sql
	 * @constructor
	 */
	Exec(sql: string): Promise<void>;
}
