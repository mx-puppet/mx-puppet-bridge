/*
Copyright 2020 mx-puppet-bridge
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import { IDatabaseConnector, ISqlRow } from "./connector";
// import { Log } from "../log";

// const log = new Log("DbReactionStore");

export interface IReactionStoreEntry {
	puppetId: number;
	roomId: string;
	userId: string;
	eventId: string;
	reactionMxid: string;
	key: string;
}

export class DbReactionStore {
	private static getFromRow(row: ISqlRow | null): IReactionStoreEntry | null {
		if (!row) {
			return null;
		}
		return {
			puppetId: Number(row.puppet_id),
			roomId: row.room_id as string,
			userId: row.user_id as string,
			eventId: row.event_id as string,
			reactionMxid: row.reaction_mxid as string,
			key: row.key as string,
		};
	}
	constructor(
		private db: IDatabaseConnector,
		private protocol: string = "unknown"
	) {}

	public async exists(data: IReactionStoreEntry): Promise<boolean> {
		return this.db.Transaction(async (tx) => {
			const stopTimer = this.db.latency.startTimer(
				this.labels("select_exists")
			);
			const exists = await tx.Get(
				`SELECT 1
                 FROM reaction_store
                 WHERE puppet_id = $pid
                   AND user_id = $uid
                   AND room_id = $rid
                   AND user_id = $uid
                   AND event_id = $eid
                   AND key = $key`,
				{
					pid: data.puppetId,
					rid: data.roomId,
					uid: data.userId,
					eid: data.eventId,
					key: data.key,
				}
			);
			stopTimer();
			return !!exists;
		});
	}

	public async insert(data: IReactionStoreEntry): Promise<boolean> {
		return this.db.Transaction(async (tx) => {
			const stopTimer = this.db.latency.startTimer(this.labels("insert"));
			if (await this.exists(data)) {
				return false;
			}
			await tx.Run(
				`INSERT INTO reaction_store
                              (puppet_id, user_id, room_id, event_id, reaction_mxid, key)
                          VALUES ($pid, $uid, $rid, $eid, $rmxid, $key)`,
				{
					pid: data.puppetId,
					uid: data.userId,
					rid: data.roomId,
					eid: data.eventId,
					rmxid: data.reactionMxid,
					key: data.key,
				}
			);
			stopTimer();
			return true;
		});
	}

	public async getFromReactionMxid(
		reactionMxid: string
	): Promise<IReactionStoreEntry | null> {
		return this.db.Transaction(async (tx) => {
			const stopTimer = this.db.latency.startTimer(
				this.labels("select_by_reaction_mxid")
			);
			const row = await tx.Get(
				"SELECT * FROM reaction_store WHERE reaction_mxid = $reactionMxid",
				{ reactionMxid }
			);
			stopTimer();
			return DbReactionStore.getFromRow(row);
		});
	}

	public async getFromKey(
		data: IReactionStoreEntry
	): Promise<IReactionStoreEntry | null> {
		return this.db.Transaction(async (tx) => {
			const stopTimer = this.db.latency.startTimer(
				this.labels("select_by_key")
			);
			const row = await tx.Get(
				`SELECT *
                 FROM reaction_store
                 WHERE puppet_id = $pid
                   AND user_id = $uid
                   AND room_id = $rid
                   AND event_id = $eid
                   AND key = $key`,
				{
					pid: data.puppetId,
					rid: data.roomId,
					uid: data.userId,
					eid: data.eventId,
					key: data.key,
				}
			);
			stopTimer();
			return DbReactionStore.getFromRow(row);
		});
	}

	public async getForEvent(
		puppetId: number,
		eventId: string
	): Promise<IReactionStoreEntry[]> {
		return this.db.Transaction(async (tx) => {
			const stopTimer = this.db.latency.startTimer(
				this.labels("select_for_event")
			);
			const rows = await tx.All(
				"SELECT * FROM reaction_store WHERE puppet_id = $puppetId AND event_id = $eventId",
				{ puppetId, eventId }
			);
			const result: IReactionStoreEntry[] = [];
			for (const row of rows) {
				const entry = DbReactionStore.getFromRow(row);
				if (entry) {
					result.push(entry);
				}
			}
			stopTimer();
			return result;
		});
	}

	public async delete(reactionMxid: string) {
		return this.db.Transaction(async (tx) => {
			const stopTimer = this.db.latency.startTimer(this.labels("delete"));
			await tx.Run(
				"DELETE FROM reaction_store WHERE reaction_mxid = $reactionMxid",
				{ reactionMxid }
			);
			stopTimer();
		});
	}

	public async deleteForEvent(puppetId: number, eventId: string) {
		return this.db.Transaction(async (tx) => {
			const stopTimer = this.db.latency.startTimer(
				this.labels("delete_for_event")
			);
			await tx.Run(
				"DELETE FROM reaction_store WHERE puppet_id = $puppetId AND event_id = $eventId",
				{ puppetId, eventId }
			);
			stopTimer();
		});
	}

	private labels(queryName: string): object {
		return {
			protocol: this.protocol,
			engine: this.db.type,
			table: "reaction_store",
			type: queryName,
		};
	}
}
