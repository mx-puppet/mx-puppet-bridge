CHANGELOG.md: CHANGELOG-draft.md
	$(TOWNCRIER) build --version=$(VERSION)

CHANGELOG-draft.md: $(glob changelog.d/*) package.json
	$(TOWNCRIER) build --version=$(VERSION) --draft | tee $@

release: CHANGELOG.md
	git add CHANGELOG.md changelog.d
	git commit -m "Release $(VERSION)"

release-draft: CHANGELOG-draft.md
